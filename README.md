# [Find Wiki Page Here](https://gitlab.com/mcgill-winterland-lab/how-to-gitlab-wiki/-/wikis/home)

# How-To Gitlab Wiki

A template wiki showing off the GitLab functionality.

This should be much better than github because it is built directly on top of 
asciidoc (so has a more fully featured rendering set there) and has inbuilt 
support for math in Markdown.

